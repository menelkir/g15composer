/* A Bison parser, made by GNU Bison 2.3.  */

/* Skeleton interface for Bison's Yacc-like parsers in C

   Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004, 2005, 2006
   Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     T_NUMBER = 258,
     T_STRING = 259,
     T_NEWLINE = 260,
     T_PIXELSET = 261,
     T_PIXELFILL = 262,
     T_PIXELRFILL = 263,
     T_PIXELOVERLAY = 264,
     T_PIXELBOX = 265,
     T_PIXELCLEAR = 266,
     T_DRAWLINE = 267,
     T_DRAWCIRCLE = 268,
     T_DRAWRBOX = 269,
     T_DRAWBAR = 270,
     T_DRAWNUM = 271,
     T_DRAWICON = 272,
     T_DRAWSPRITE = 273,
     T_WBMPSPLASH = 274,
     T_WBMPLOAD = 275,
     T_MODECACHE = 276,
     T_MODEREV = 277,
     T_MODEXOR = 278,
     T_MODEPRI = 279,
     T_FONTLOAD = 280,
     T_FONTPRINT = 281,
     T_TEXTSMALL = 282,
     T_TEXTMED = 283,
     T_TEXTLARGE = 284,
     T_TEXTOVERLAY = 285,
     T_KEYL = 286,
     T_KEYM = 287,
     T_LCDBL = 288,
     T_LCDCON = 289,
     T_SCREENNEW = 290,
     T_SCREENCLOSE = 291
   };
#endif
/* Tokens.  */
#define T_NUMBER 258
#define T_STRING 259
#define T_NEWLINE 260
#define T_PIXELSET 261
#define T_PIXELFILL 262
#define T_PIXELRFILL 263
#define T_PIXELOVERLAY 264
#define T_PIXELBOX 265
#define T_PIXELCLEAR 266
#define T_DRAWLINE 267
#define T_DRAWCIRCLE 268
#define T_DRAWRBOX 269
#define T_DRAWBAR 270
#define T_DRAWNUM 271
#define T_DRAWICON 272
#define T_DRAWSPRITE 273
#define T_WBMPSPLASH 274
#define T_WBMPLOAD 275
#define T_MODECACHE 276
#define T_MODEREV 277
#define T_MODEXOR 278
#define T_MODEPRI 279
#define T_FONTLOAD 280
#define T_FONTPRINT 281
#define T_TEXTSMALL 282
#define T_TEXTMED 283
#define T_TEXTLARGE 284
#define T_TEXTOVERLAY 285
#define T_KEYL 286
#define T_KEYM 287
#define T_LCDBL 288
#define T_LCDCON 289
#define T_SCREENNEW 290
#define T_SCREENCLOSE 291




#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
#line 45 "g15composer.y"
{
	int number;
	char *string;
	struct strList *strList;
}
/* Line 1489 of yacc.c.  */
#line 127 "g15composer.tab.h"
	YYSTYPE;
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
# define YYSTYPE_IS_TRIVIAL 1
#endif



